﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsPOP3
{
    public partial class MainForm : Form
    {
        private Pop3Mail pop3mail;
        public MainForm()
        {
            InitializeComponent();
            pop3mail = new Pop3Mail("pop.gmail.com", 995);
        }

        private void btnGetMail_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtLogin.Text))
            {
                MessageBox.Show("Please enter login");
                return;
            }
            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Please enter password");
                return;
            }
            if (!Int32.TryParse(txtMailId.Text,out int id))
            {
                MessageBox.Show("Please enter mail id");
                return;
            }

            txtMail.Text = pop3mail.GetMail(txtLogin.Text, txtPassword.Text, id);
        }
    }
}
