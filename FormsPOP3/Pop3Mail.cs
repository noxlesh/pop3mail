﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace FormsPOP3
{
    class Pop3Mail
    {
        private IPAddress addr;
        private int port;

        public Pop3Mail(string host, int port)
        {
            addr = Dns.GetHostAddresses(host)[0];
            this.port = port; 
        }

        public string GetMail(string user, string password, int mail)
        {
            TcpClient tcpClient;
            StreamReader sr;
            StreamWriter sw;
            SslStream sslStream;

            tcpClient = new TcpClient();
            tcpClient.Connect(addr, port);
            sslStream = new SslStream(tcpClient.GetStream());
            sslStream.AuthenticateAsClient("pop.gmail.com", new X509CertificateCollection(), SslProtocols.Tls, true);
            sr = new StreamReader(sslStream);
            sw = new StreamWriter(sslStream);
            sw.AutoFlush = true;

            sr.ReadLine();
            sw.WriteLine("USER noxlesh@gmail.com");
            sr.ReadLine();
            sw.WriteLine("PASS noxlesh1986");
            sr.ReadLine();
            sw.WriteLine("STAT");
            sr.ReadLine();
            sw.WriteLine("RETR "+ mail);
            sw.WriteLine("QUIT");
            string mailStr = sr.ReadToEnd();
            tcpClient.Close();

            return  mailStr;
        }
    }
}
